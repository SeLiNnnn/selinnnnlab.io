---
title: 浅谈JavaScript闭包
date: 2021-04-04 21:00
tags:
- JavaScript
- 闭包
categories:
- Note
---


# 浅谈JavaScript闭包

> 本文涉及闭包、执行上下文、变量对象、作用域链、V8引擎垃圾回收机制、内存泄露，篇幅较长，写得吐血，看得迷惑

## 1、什么是闭包？

**可以访问自由变量的函数。《JavaScript权威指南》**

**自由变量是指在函数中使用，但既不是函数参数，也不是函数的局部变量。**

所以得出结论：

闭包 = 函数 +  函数能访问的自由变量

举个栗子A：

```javascript
var scope = 'global scope';
function fn() {
	console.log(scope); // window.scope
}
```

如上变量a既不是函数参数，也不是函数局部变量，所以a就是自由变量。

故从技术角度来说，JS所有函数都是闭包。但从实践角度，ECMAScript中认为闭包是指：

- **即使创建该函数的上下文已经被销毁，它仍然存在（比如，内部函数从外部函数中返回）**
- 可以访问自由变量。

改造一下栗子B：

```javascript
// 执行foo()返回???
var scope = 'global scope';
function fn() {
	var scope  = 'local scope'
	function f() {
		return scope;
	}
	return f;
}
var foo = fn();
foo();
```

改造后的栗子B返回结果是"local scope"，这就是符合实践中闭包概念的第一条了：当执行内部函数f()时，外部fn()的执行上下文已经被销毁，但内部f()依然可以访问到外部函数的局部变量。

接下来详细分析，为什么是这样？

## 2、执行上下文

### 2-1 JS执行顺序

起初，我们以为JS是按顺序执行，如:

```javascript
var fn = function() {
	console.log("fn1")
}
fn(); // fn1

var fn = function() {
	console.log("fn2")
}
fn(); // fn2
```

然而如下代码：

```javascript
functionfn() {
	console.log("fn1")
}
fn(); // fn2

functionfn() {
	console.log("fn2")
}
fn(); // fn2
```

所以我们能发现，JS引擎并不是一行行执行，而是一段段分析和执行，上面两段代码就存在着变量提升和函数提升。

当要执行一段可执行代码(executable code)时，JS会需要做一些准备工作，这个准备工作，就可以理解为执行上下文(executioong context)。

JS执行代码时会维护一个执行上下文栈(ECStack)用来管理众多的执行上下文，我们使用一个数组来表示执行上下文栈:

```javascript
ECS = [];
```

当JS开始要解释执行代码时，最先遇到的就是全局变量，而当所有代码都执行完毕，ECS才会被清空，所以程序结束前，栈底会始终存在全局上下文:

```javascript
ECS  = [globalContext];
```

看一段代码：

```javascript
function f2() {
	console.log("f2")
}
function f1() {
	fn2()
}
f1()
```

当执行一个函数的时候，就会创建一个执行上下文，并将其压入ECS，当函数执行完毕，就会将函数的执行上下文从ECS中弹出。所以分析这段代码：

```javascript
// 伪代码
// f1()
ECS.push(f1 f1Context)

// f1中使用了f2
ECS.push(f2 f2Context)

// f2执行完毕
ECS.pop(）

// f1执行完毕
ECS.pop()

// js继续执行其他代码，但ECS栈底永远存在globalContext
```

### 2-1、作用域链

由此，回顾刚才改造后的🌰栗子B:

```javascript
// 执行foo()返回???
var scope = 'global scope';
function fn() {
	var scope  = 'local scope'
	function f() {
		return scope;
	}
	return f;
}
var foo = fn();
foo();
```

可以分析出JS执行过程：

1. 执行全局代码，创建全局执行上下文，压入ECS:

```javascript
ECS = [
	globalContext
]
```

2. 全局上下文初始化

```javascript
globalContext = {
	VO: [global],
  Scope: [globalContext.VO],
  this: globalContext.VO
}
```

3. 初始化的同时，函数foo被创建并保存作用域链到函数的内部属性[scope]

```javascript
foo.scope = [
	globalContext.VO
]
```

PS：这里可以说明，函数的作用域在函数定义的时候就决定了，因为JS采用的是词法作用域(lexical scoping)也就是静态作用域，而动态作用域则是在函数调用时才决定的。

静态作用域与动态作用域的区别:

```javascript
var value = 1;

function foo() {
    console.log(value);
}

function bar() {
    var value = 2;
    foo();
}

bar();

// 结果是 ???
```

因为JS是静态作用域，所以结果是1。

当函数创建的时候，就会保存所有父变量对象到函数的[scope]属性中，可以把[scope]理解为是所有父变量对象的层级链，但是注意：[scope] 并不代表完整的作用域链！

4. 执行函数foo，创建其执行上下文并压入ECS：

```javascript
ECS = [
	fooContext,
	globalContext
]
```

5. 初始化函数foo执行上下文：

- 复制函数[scope]属性创建作用域链
- 用arguments创建活动对象
- 初始化活动对象，加入形参、函数声明、变量声明
- 将活动对象压入foo作用域链顶端

同时，内部函数f被创建，保存作用域链到f函数的内部属性[scope]

```javascript
fnContext = {
  AO: { 
		arguments: {
			length: 0;
		},
		scope: undefined,
		f: reference to function f() {}
	},
	Scope: [AO, globalContext.VO],
	this: undefined
}
```

这里需要说明的是，活动对象和变量对象其实是一个东西，只是变量对象是规范上所说的，不可在 JavaScript 环境中访问，只有到当进入一个执行上下文中，这个执行上下文的变量对象才会被激活，变为活动变量，而只有被激活的变量对象，也就是活动对象上的各种属性才能被访问。

- 进入执行上下文（代码执行时）

当进入执行上下文时，这时还没有执行代码，变量对象会包括：

1. 函数的所有形参 (如果是函数上下文)
    - 由名称和对应值组成的一个变量对象的属性被创建（通过函数的 arguments 属性初始化，arguments 属性值是 Arguments 对象）
    - 没有实参，属性值设为 undefined
2. 函数声明
    - 由名称和对应值（函数对象(function-object)）组成一个变量对象的属性被创建
    - **如果变量对象已经存在相同名称的属性，则完全替换这个属性（函数提升）**
3. 变量声明
    - 由名称和对应值（undefined）组成一个变量对象的属性被创建；
    - **如果变量名称跟已经声明的形式参数或函数相同，则变量声明不会干扰已经存在的这类属性（变量提升）**

6. 函数foo执行完毕，将foo执行上下文弹出

```javascript
ECS = [
	globalContext
]
```

7. 执行函数f, 创建f函数执行上下文，压入ECS

```javascript
ECS = [
	fContext,
	globalContext
]
```

8. 初始化函数f执行上下文，与第5步相同：

- 复制函数[scope]属性创建作用域链
- 用arguments创建活动对象
- 初始化活动对象，加入形参、函数声明、变量声明
- 将活动对象压入f作用域链顶端

```javascript
fContext = {
	AO: {
		arguments: {
			length: 0
		}
	},
	Scope: [AO,fn.AO,globalContext.VO],
	this: undefined
}
```

9. 执行函数f，需返回scope，此时就会发现，当执行内部函数f时，外部函数foo已经被销毁了。但是！！从上面👆🏻那段代码可以看到，f执行上下文维护了一个作用域链，所以f沿着作用域链寻找到了foo.AO的值，尽管fooContext就销毁了，但实际上依然存在于内存中，所以函数f执行完毕会返回"local scope"，一旦找到了该值，就不会再往上寻找了。

 此f执行完毕，f函数执行上下文从ECS中弹出:

```javascript
ECS = [
	globalContxt
]
```

再看一个栗子C：

```javascript
// 执行foo()返回???
var scope = 'global scope';
function fn() {
	var scope  = 'local scope'
	function f() {
		return scope;
	}
	return f(); // 注意这一行
}
var foo = fn();
foo();
```

结果是和栗子B一样，返回"local scope"，虽然结果一致，但它们执行上下文的情况是不一样的，按照刚才的思路来分析：

```javascript
// 1第一步相同
// 因为需要执行foo()，所以将foo()的上下文压入栈中
ECS.push(foo fooContext)
// 2
// 因为需要执行f()，所以将fn()的上下文压入栈中，注意到这里的区别了吗
ECS.push(f fContext)
// 3
// 执行f()并弹出
ECS.pop()
// 4
// 执行foo并弹出
ECS.pop()
```

这就是闭包的原理以及为什么会形成闭包的原因，那么，可以进一步思考，为什么外部函数的执行上下文销毁了作用域链中的活动对象却依然存在于内存中呢？JS是如何对内存进行管理的呢？

## 3、V8引擎垃圾回收机制

这就要说到垃圾回收(garbage collection)了，Chrome的V8是如何进行垃圾回收的，理解了gc才能更深入理解作用域链与闭包。

### 3-1 为什么需要垃圾回收

通过前文，我们已经知道，JS引擎执行一段可执行代码的时候，遇到函数，会为其创建执行上下文，并压入执行上下文栈中，函数的作用域链中包含了该函数中声明的所有变量，当函数执行完毕后，其执行上下文被弹出，作用域也随之销毁，其中的变量会被自动回收，内存空间也将被释放。

如果在销毁作用域的过程中，变量不被回收，长久占用内存，那么必然会导致内存占用暴增，进而引发内存泄露等问题，因此内存在使用完毕之后理当归还给操作系统以保证内存的重复利用。

那么JS中的变量是如何保存在内存中的呢？那就要先来看看JS的几种数据类型。

### 3-1-1 JS数据类型

大家都知道，JS中的数据类型分为基础类型和引用类型：

基础类型：

number,string,boolean,null,undefined,symbol

引用类型：

object,function,array,reg,date

那么这两种数据类型是如何存储的呢?

基础类型是存在于栈内存中`(不包含闭包中变量)`，对基础数据的赋值会在内存中开辟出新的区域:

```javascript
var a = 1;
var b = a;
b = 2;
console.log(a); // 1
```

![Untitled.png](/images/浅谈JavaScript闭包/Untitled.png)

而引用类型的值是对象，存在于堆(Heap)内存中，只会在栈内存中保存引用类型的标识符以及对象在堆内存中的地址，通过在栈内存中指针指向其在堆内存中的起始地址。对引用类型的赋值，实际是将指针指向相同的堆内存地址。

```javascript
var o = {name:'Hermonie'};
var obj = o;
obj.name = 'Draco';
console.log(o.name); // Draco
```
![Untitled1.png](/images/浅谈JavaScript闭包/Untitled1.png)


PS：特别注意，这里又与闭包的知识点串起来了，闭包中的变量并不保存在栈内存中，而是保存在堆内存中。如果闭包中的变量保存在了栈内存中，随着外层函数的执行上下文被销毁销毁，变量肯定也会被销毁，但如果保存在了堆内存中，内部函数仍能访问外层已销毁函数中的变量。

接下来我们就来详细了解，这两种数据类型是如何被回收的。

### 3-2 V8垃圾回收策略

V8的垃圾回收策略主要是基于**分代式垃圾回收机制**，其根据**对象的存活时间**将内存的垃圾进行不同的分代，然后对不同的分代采用不同的垃圾回收算法。

### 3-2-1 V8内存结结构

这里只探讨和垃圾回收过程紧密相关的新生代和老生代。

`新生代(new_space)`：大多数的对象开始都会被分配在这里，这个区域相对较小但是垃圾回收特别频繁，该区域被分为两半，一半用来分配内存，另一半用于在垃圾回收时复制需要保留的对象。

`老生代(old_space)`：新生代中的对象在存活一段时间后会被转移到老生代内存区，相对于新生代该内存区域的垃圾回收频率较低。老生代又分为`老生代指针区`和`老生代数据区`，前者包含大多数可能存在指向其他对象的指针的对象，后者只保存原始数据对象，这些对象没有指向其他对象的指针。

![Untitled2.png](/images/浅谈JavaScript闭包/Untitled2.png)


上图中的带斜纹的区域代表暂未使用的内存，新生代(new_space)被划分为了两个部分，其中一部分叫做inactive new space，表示暂未激活的内存区域，另一部分为激活状态。

### 3-3 新生代

在V8引擎的内存结构中，新生代主要用于存放存活时间较短的对象，在新生代中主要采用了Cheney算法进行垃圾回收。

可以简单理解为是一种牺牲空间换取时间的复制算法，会在新生代中划分出两块区域:处于激活状态的`From`区域和未激活的`To`区域。

这两个空间中，始终只有一个处于使用状态，另一个处于闲置状态。在代码中声明的变量对象VO首先会被分配到`From`区域，当进行垃圾回收时，如果`From`空间中还有活动对象AO，则会被复制到`To`空间进行保存，而非活动的对象会被自动回收。当复制完成后，`From`空间和`To`空间会完成一次互换，`To`空间会变为新的`From`空间，原来的`From`空间则变为`To`空间。

那么变量对象什么时候会被转移到老生代中呢？

### 3-4 对象晋升

当一个对象经过多次的复制依然存活，就会认为它是一个生性周期较长的对象，在下一次的垃圾回收中，就会被转移到老生代中，这种转移的过程就称为`晋升`。

对象晋升满足以下任意条件：

- 对象是否已经过一次Cheney算法
- `To`区域内存占用超过25%

之所以有`25%`的内存限制是因为`To`区域经历过一次Cheney算法后会和`From`空间互换，变为`From`空间。而后续的内存分配都是在`From`空间中进行的，如果内存使用过高甚至溢出，则会影响后续对象的分配，因此超过这个限制之后对象会被直接转移到老生代来进行管理。

### 3-5 老生代

在老生代中依然管理着大量活动对象，老生代中采用新的算法`Mark-Sweep(标记清除)`和`Mark-Compact(标记整理)`来进行管理。

`Mark-Sweep(标记清除)`分为标记和清除两个阶段，在标记阶段会遍历堆内存中的所有对象并标记存活对象，在清除阶段中，会将死亡的对象进行清除。`Mark-Sweep(标记清除)`算法主要是通过判断某个对象是否可以被访问到，从而知道该对象是否应该被回收，具体步骤如下：

- 垃圾回收器会在内部构建一个根列表，用于从根节点出发去寻找那些可以被访问到的变量。比如在JavaScript中，window全局对象可以看成一个根节点。
- 垃圾回收器从所有根节点出发，遍历其可以访问到的子节点，并将其标记为活动对象，根节点不能到达的地方即为非活动的，将会被视为垃圾。
- 垃圾回收器将会释放所有非活动的内存块，并将其归还给操作系统。

以下几种情况都可以作为根节点：

- 全局对象
- 本地函数的局部变量和参数
- 当前嵌套调用链上的其他函数的变量和参数

但是`Mark-Sweep`算法存在一个问题，就是在经历过一次标记清除后，内存空间可能会出现不连续的状态，因为清理的对象内存地址可能就是不连续的，就会出现内存碎片的问题，为了解决这一问题，便诞生了`Mark-Compact(标记整理)`算法，用来解决内存碎片化的问题。在回收死亡对象后，对还存活的对象进行整理，整理过程中会将活动对象往堆内存的一端进行移动，移动完成后再清理掉除了AO边界内的其他内存区域，这样就完成了一次老生代的垃圾回收过程。

*扩展阅读：*

但由于JS的单线程机制，垃圾回收的过程会阻碍主线程同步任务的执行，待执行完垃圾回收后才会再次恢复执行主任务的逻辑，这种行为被称为`全停顿(stop-the-world)`。在标记阶段同样会阻碍主线程的执行，一般来说，老生代会保存大量存活的对象，如果在标记阶段将整个堆内存遍历一遍，那么势必会造成严重的卡顿。
因此，为了减少垃圾回收带来的停顿时间，V8引擎又引入了`Incremental Marking(增量标记)`的概念，即将原本需要一次性遍历堆内存的操作改为增量标记的方式，先标记堆内存中的一部分对象，然后暂停，将执行权重新交给JS主线程，待主线程任务执行完毕后再从原来暂停标记的地方继续标记，直到标记完整个堆内存。这个理念其实有点像`React`框架中的`Fiber`架构，只有在浏览器的空闲时间才会去遍历Fiber Tree执行对应的任务，否则延迟执行，尽可能少地影响主线程的任务，避免应用卡顿，提升应用性能。
得益于增量标记的好处，V8引擎后续继续引入了`延迟清理(lazy sweeping)`和`增量式整理(incremental compaction)`，让清理和整理的过程也变成增量式的。同时为了充分利用多核CPU的性能，也将引入并行标记和并行清理，进一步地减少垃圾回收对主线程的影响，为应用提升更多的性能。

那么如果一些对象变量长期存活，无法被垃圾回收，又会导致什么问题呢？

## 4、内存泄露

### 4-1 内存的生命周期

内存的生命周期一般可分为三个：

- 分配期

    分配所需要的内存

- 使用期

    使用分配到的内存（读、写）

- 释放期

    不需要时将其释放和归还

内存分配 -> 内存使用 -> 内存释放。

### 4-2 什么是内存泄露？

> 在计算机科学中，内存泄漏指由于疏忽或错误造成程序未能释放已经不再使用的内存。内存泄漏并非指内存在物理上的消失，而是应用程序分配某段内存后，由于设计错误，导致在释放该段内存之前就失去了对该段内存的控制，从而造成了内存的浪费。

简单来说，就是无用内存得不到释放，长久占用使得内存持续增加，导致整个系统卡顿或崩溃。

在JS中，内存是由JS引擎自动为我们分配的，无需手动去分配内存。

那么结合上文的垃圾回收机制，我们就能明白，如果变量长期存在无法触发gc势必就会造成内存泄露。

### 4-1 如何防止内存泄露

- 1尽可能少地创建全局变量

我们已经知道，当进行垃圾回收时，在老生代的标记阶段因为`window`对象可以作为根节点，在`window`上挂载的属性均可以被访问到，并将其标记为活动的从而常驻内存，因此也就不会被垃圾回收，只有在整个进程退出时全局作用域才会被销毁。如果必须要使用全局变量，也一定要在全局变量使用完毕后将其设置为`null`从而触发gc。

- 2减少使用闭包

再结合本文最初探讨闭包的时候，举的栗子B，即使外部函数的执行上下文被销毁了，内部函数依然通过作用域链访问到了外部函数的活动变量，这也就意味着这些活动变量是无法被垃圾回收的，只有当取消掉对内部函数的引用时才会进入gc。

- 3手动清除定时器

当在使用setTimeout和setInterval的时候，如果忘记清除定时器，使得某项操作无限执行，就会导致内存泄露:

```javascript
const numbers = [];
const foo = function() {
    for(let i = 0;i < 100000;i++) {
        numbers.push(i);
    }
};
window.setInterval(foo, 1000);
```

- 使用弱引用(WeakMap)

`弱引用`是指垃圾回收的过程中不会将键名对该对象的引用考虑进去，只要所引用的对象没有其他的引用了，垃圾回收机制就会释放该对象所占用的内存。这也就意味着我们不需要关心WeakMap中键名对其他对象的引用，也不需要手动地进行引用清除。

看两个栗子：

```javascript
let set = new Set();
let val = {name:'selin'};
set.add(val);
val = null;
```

在这个栗子🌰中，即使执行`val = null`也存在内存泄露，因为set中依然存有对val的引用。

改为弱引用：

```javascript
let weakSet = new WeakSet();
let val = {name:'lucy'};
weakSet.add(val)

val = null;
```

ES6中新增的WeakSet和WeakMap都是弱引用，垃圾回收时就不会再考虑这个引用是否存在了。

- 清除DOM引用

以往操作DOM元素时，为了避免多次获取DOM元素，可能会将DOM元素存储在一个数据字典中：

```javascript
const elements = {
    button: document.getElementById('button')
};

function removeButton() {
    document.body.removeChild(document.getElementById('button'));
}
```

在这个栗子中，想调用removeButton方法来清除button元素，但由于在elements字典中存在对button元素的引用，所以即使通过removeChild方法移除了button元素，它其实依旧存储在内存中无法得到释放，只有手动清除对button元素的引用时，才会被垃圾回收。

至此，是我最近在看了一些文章后，对各个问题的回顾和思考，以及对一些难以表达清楚的内容进行了摘抄，梳理后才发觉知识之间从来都是融会贯通的。欢迎留言与我探讨👏🏻有些内容看了很多次还是没能理解透彻。

PS：

- 在Notion中查看本文: [浅谈JavaScript闭包]()

- 更多细节性的内容还是建议参考以下文章，反复阅读这些文章使我收获颇多

参考链接：

[JavaScript深入之闭包](https://github.com/mqyqingfeng/Blog/issues/9)

[JavaScript深入之变量对象](https://github.com/mqyqingfeng/Blog/issues/5)

[JavaScript深入之作用域链](https://github.com/mqyqingfeng/Blog/issues/6)

[JavaScript深入之执行上下文栈](https://github.com/mqyqingfeng/Blog/issues/4)

[JavaScript深入之执行上下文](https://github.com/mqyqingfeng/Blog/issues/8)

[一文搞懂V8引擎的垃圾回收](https://juejin.cn/post/6844904016325902344)

[深入了解JavaScript内存泄露](https://segmentfault.com/a/1190000020231307)
