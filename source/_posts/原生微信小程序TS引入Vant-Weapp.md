---
title: 原生微信小程序TS引入Vant-Weapp 
date: 2020-12-06
tags:
    - 微信小程序
    - Vant-Weapp
categories:
    - Record
---

摔... 真的是踩了个大坑。折腾了几个小时，终于无错运行成功。

首先一点，vant-weapp官方教程是没有错的！没有错的！没有错的！如果和我一样首次使用ts+vantweapp建议就参考教程即可。

https://youzan.github.io/vant-weapp/#/quickstart

但要注意一些教程中没有说明的坑：

1.如果不熟悉目录结构，不需要挪动node_modules和package.json的位置,也不需要在miniprogram下去生成package.json文件（如果能保证配置成功就可以这样,因为我自己也看了很多别的文章，但当时还存在别的报错无法解决）

就用小程序新建时，官方给的目录结构，也就是说，直接在项目根目录下安装vant-weapp和miniprogram-api-typings即可

![img.png](/images/原生微信小程序TS引入Vant-Weapp1/img.png)

但是一定按照步骤三进行修改npm包的引用，只要在开发工具构建npm时，能够成功生成miniprogram_npm并且里面就是@vant/weapp/各种组件的文件夹，那就说明路径是正确的，构建也是成功的。

2.注意typings文件夹
在官方教程的第五步特别有提到，安装miniprogram-api-tyings可以更好的适配ts

![img_1.png](/images/原生微信小程序TS引入Vant-Weapp1/img_1.png)

这一步时，需删除ts项目自带的typings文件夹，否则会在控制台报很多重复定义类型的错误。

这一步可能还有别的问题，例如找不到IAppOption/current/Omit如果出现这几种问题，参见https://github.com/wechat-miniprogram/api-typings/issues/170

我提了issue，并发现前两种是由于api-typings和ts版本过低导致，目前ts版本为4.1.2，项目自带的却是3.3333，会找不到Omit这个类型。而api-typings（一定要根据官方教程安装在devDependencies，否则会报错找不到index.d.ts的入口）版本也需要更新，目前是3.2.0，可在npm官网确认，因为current的问题已经得到了修复。

关于IAppOption，的确是没有定义，可从微信本身的typings中拷贝一份定义出来，放在inde.d.ts即可。

![img_2.png](/images/原生微信小程序TS引入Vant-Weapp1/img_2.png)

这里需要注意的是，如果想要在globalData加入其他属性，在ts中必须修改这个interface否则会报错。但我认为不应该把其他东西放在这里面，毕竟并不应该去修改已经定义好的类型，如果万不得已，可以写成可选属性，类似于 [propName:string]:any

如遇到下图红色的错误，不用管他，解决好api-typings里的问题，这个会在后面编译成功的

![img_3.png](/images/原生微信小程序TS引入Vant-Weapp1/img_3.png)

折腾这些问题直到没有任何报错(控制台-设置-勾选上Preserve log可以更好的查看报错)就可以接着按照官方教程里的使用示例进行使用组件了。

![img_4.png](/images/原生微信小程序TS引入Vant-Weapp1/img_4.png)

最后放一下项目结构图：

![img_5.png](/images/原生微信小程序TS引入Vant-Weapp1/img_5.png)
