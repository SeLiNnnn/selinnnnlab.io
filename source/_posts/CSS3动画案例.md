---
title: CSS3动画案例

date: 2021-05-25 18:20:42

tags:

- CSS3

categories:

- Notes

---

## 一、3D翻转

- 效果图：
![/images/CSS3动画案例/3D翻转.gif](/images/CSS3动画案例/3D翻转.gif)

- 代码：

```html

<div class="stage">
	<div class="box">
		<div class="cover front">
			<img alt="coffee" src="./images/1.jpg">
		</div>
		<div class="cover back">
			<h3>卡布奇诺</h3>
		</div>
	</div>
	<div class="box">
		<div class="cover front">
			<img alt="coffee" src="./images/2.jpg">
		</div>
		<div class="cover back">
			<h3>卡布奇诺</h3>
		</div>
	</div>
	<div class="box">
		<div class="cover front">
			<img alt="coffee" src="./images/3.jpg">
		</div>
		<div class="cover back">
			<h3>卡布奇诺</h3>
		</div>
	</div>
	<div class="box">
		<div class="cover front">
			<img alt="coffee" src="./images/4.jpg">
		</div>
		<div class="cover back">
			<h3>卡布奇诺</h3>
		</div>
	</div>
</div>

```

```css
  * {
  padding: 0;
  margin: 0;
}

body {
  background-color: #0d3462;
}

.stage {
  margin: 100px;
}

img {
  display: block;
  width: 100%;
  height: 100%;
}

.box {
  position: relative;
  float: left;
  width: 170px;
  height: 190px;
  cursor: pointer;
  transform-style: preserve-3d;
  transition: 1.5s linear;
}

.box:hover {
  transform: rotateY(180deg);
}

.cover {
  position: absolute;
  width: 100%;
  height: 100%;
}

.back {
  transform: rotateY(180deg);
  background-color: antiquewhite;
  text-align: center;
  line-height: 190px;

```

## 二、文字遮罩

- 效果图：
  ![/images/CSS3动画案例/文字遮罩.gif](/images/CSS3动画案例/文字遮罩.gif)

- 代码：

```html

<div class="stage">
	<div class="container">
		<div class="front">
			<img alt="img" src="./images/1.png">
		</div>
		<div class="back">
			The Chamber of Secrets
		</div>
	</div>
</div>

```

```css
   * {
  margin: 0;
  padding: 0;
}

.stage {

}

.container {
  position: relative;
  overflow: hidden;
  width: 300px;
  height: 300px;
  cursor: pointer;
}

.container:hover .back {
  top: 0;
}

.front {
  position: absolute;
  width: 100%;
  height: 100%;
}

.front img {
  width: 100%;
  height: 100%;
}

.back {
  position: absolute;
  top: 300px;
  width: 100%;
  height: 100%;
  color: white;
  background-color: rgba(0, 0, 0, .7);
  transition: 1s;
  text-align: center;
  line-height: 300px;
}

```

## 三、徽章扫光效果

- 效果图：
  ![/images/CSS3动画案例/徽章扫光.gif](/images/CSS3动画案例/徽章扫光.gif)

- 代码：

```html

<div class="box"></div>

```

```css

.box {
  position: relative;
  width: 200px;
  height: 200px;
  border-radius: 50%;
  background: url("./images/2.png") 280px;
}

.box:after {
  content: "";
  position: absolute;
  top: 0;
  width: 200vw;
  height: 35px;
  border-radius: 50%;
  background-color: rgba(255, 255, 255, .4);
  transform: rotate(45deg);
  animation: 2s ease-in infinite scan-light;
}

@keyframes scan-light {
  from {
    right: -100vw;
  }
  to {
    right: 40vw;
  }
}

```
