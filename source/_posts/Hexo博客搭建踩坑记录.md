---
title: Hexo博客搭建踩坑记录
date: 2020-07-28 01:00:21
tags: Hexo
categories: Record
---

## 一、 Hexo更换主题Chic后提交至远程仓库显示空白页，本地显示正常
* 问题原因： 搜过类似解答，基本是以前存在的问题，类似于Github对文件名的控制，故而需要修改theme下的文件夹名。好在如今主题本身几乎都已修改相应的文件夹，所以不再存在以前的问题。
真正的原因其实很简单，***是因为主题仓库本身也是git仓库，在自己的博客根目录下没有被添加，导致提交至远程仓库的主题文件夹是空的***。

* 解决方法:
```
git rm --cached /theme/Chic -r
git add /theme/Chic
```
删除已有的主题缓存重新添加至git仓库即可，提交远程后应该就能看到主题文件夹下的内容了。

## 二、 使用Gitlab CI自动部署至Gitlab Pages

1. 新建一个 repository,命名为<你的 GitLab 用户名>.gitlab.io，这样便可通过<你的 GitLab 用户名>.gitlab.io直接访问。
2. 在根目录中新建 .gitlab-ci.yml 文件：

```
image: node:10
cache:
  paths:
    - node_modules/

before_script:
  - npm install hexo-cli -g
  - npm install

pages:
  script:
    - hexo clean # 最好每次部署前都清理一下
    - hexo generate
  artifacts:
    paths:
      - public
  only:
    - master
```

3. 推送到 repository 中。默认情况下不应该 public 目录将不会被推送到 repository 中，检查 .gitignore 文件中是否包含 public 一行。（默认都有）
4. 代码推送后，GitLab CI 应该会自动开始运行，构建成功以后就可在 https://<你的 GitLab 用户名>.gitlab.io 查看博客。（可在仓库的CI / CD一栏中查看部署）

**相关资料**
[将 Hexo 部署到 GitLab Pages]('https://hexo.io/zh-cn/docs/gitlab-pages')
