---
title: 《JavaScript设计模式》

date: 2020-07-24 11:34:48

tags:
    - JavaScript
    - Design Pattern

categories: 
    - Read
---

# JS设计模式读书笔记与代码实现

> 本文内容来自阅读张容铭《JavaScript设计模式》的收获，以及书中部分代码的实现。

## 第一章

> 需求: 自主实现验证姓名、邮箱、密码

**初始写法**
```
/**
 * Stage 1 
 * 初始写法  
 * 缺点：创建了太多全局变量
 */
function checkName() {
  //  验证姓名
}

function checkEmail() {
  //  验证邮箱
}

function checkPassword() {
  //  验证密码
}
```

**用对象收编变量**
```
/**
 * Stage 2 用对象收编变量
 * 优点： 只声明了一个变量
 */
let CheckObject = {
  checkName: function () {
    //  验证姓名
  },
  checkEmail: function () {
    //  验证邮箱
  },
  checkPassword: function () {
    //  验证密码
  }
}
CheckObject.checkName(); // 使用.方法来使用
```

**类创建对象**
```
/**
 * Stage 5 类创建对象
 * 使用new创建对象
 * 缺点： 每次创建都会通过this复制新的方法 造成了许多消耗
 */
let CheObj = function () {
  this.checkName = function () {
    //  验证姓名
  }
  this.checkEmail = function () {
    //  验证邮箱
  }
}
let b = new CheObj();
b.checkName();
```

**原型创建对象**
```
/**
 * Stage 6
 * 使用原型创建对象
 * 创建出了的对象所拥有的方法都是同一个，因都依赖原型链依次寻找
 * 缺点：写了很多遍prototype
 */
let ChekObj = function () {
}
ChekObj.prototype.checkName = function () {
  //  验证姓名
}
ChekObj.prototype.checkEmail = function () {
  //  验证邮箱
}
```

***JS中的this指向当前对象***


**对象的链式调用**
```
/**
 * Stage 8 对象的链式调用
 */
let CheObjectObjLine = {
  checkName: function () {
    return this;
  },
  checkEmail: function () {
    return this;
  }
}
// 使用：
CheObjectObjLine.checkName().checkEmail()
```


**原型的链式调用**
```
/**
 * Stage 9 原型的链式调用
 */

let CheObjectLine = function () {

}
CheObjectLine.prototype = {
  checkName: function () {
    return this
  },
  checkEmail: function () {
    return this
  }
}
// 使用:
let d = new CheObjectLine();
d.checkEmail().checkEmail();
```


**函数的祖先**
```
/**
 * Stage 10 函数的祖先
 * 缺点： 污染原声对象Function
 */
Function.prototype.checkEmail = function () {

}
// 函数式使用
let f = function () {

}
f.checkEmail()

// 类式使用
let f2 = new Function()
f2.checkEmail()
```

**抽象一个统一添加方法的功能方法**
```
/**
 * Stage 11 抽象一个统一添加方法的功能方法
 */
Function.prototype.addMethod = function (name, fn) {
  this[name] = fn
}
let methods = new Function();
methods.addMethod('checkName', function () {
  // 验证姓名
})
methods.addMethod('checkEmail', function () {
  // 验证邮箱
})
methods.checkName()
methods.checkEmail()
```

**类式链式调用 -> 12**
```
/**
 * Stage 13 类式链式调用12
 */
Function.prototype.addMethodFn = function (name, fn) {
  this.prototype[name] = fn
  return this;
}
let Methods = function () {

}
Methods.addMethodFn('checkName', function () {
  console.log('check name');
  return this
}).addMethodFn('checkPassword', function () {
  console.log('check pwd');
  return this
})
let m = new Methods();
m.checkPassword().checkName()
```

---

## 章节提问

> 1. 真假对象一节, 如何实现链式调用？

**我的解答：**
```
let CheckObLine = function () {
  return {
    checkName: function () {
      console.log('test name');
      return this
    },
    checkEmail: function () {
      console.log('test email');
      return this
    }
  }
}
let t = CheckObLine()
t.checkName().checkEmail()
```

> 3. 定义一个既可为原型添加方法又可为自身添加方法的addMethod方法
**我的解答：也许不太对**
```
Function.prototype.addMethodByType = function (name, fn, type) {
  this[name] = fn;
  if (type === 'prototype') {
    this.prototype[name] = fn
    return this
  } else if (type === 'self') {
    this[name] = fn
    return this
  } else {
    console.log('类型错误');
    return this
  }
}
let test = function () {
}
test.addMethodByType('checkPwd', function () {
  console.log('check by type');
  return this
}, 'self').addMethodByType('throwErr', function () {
  console.log('err');
  return this
}, 'err')
test.checkPwd().throwErr()
```

--- 

## 传送门
完整代码请戳[这里✈]('https://github.com/SeLiNnnn/JSDesignPattern')
