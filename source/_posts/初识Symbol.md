---
title: Symbol 
date: 2020-09-28
tags:
    - ES6
categories:
    - Note
---
# 初识Symbol

## 消除魔术字符串



```
const shapeType = {
  triangle: Symbol(),
  square: Symbol()
}

function caclArea(shape, w, h) {
  let area = 0
  switch (shape) {
    case shapeType.triangle:
      area = 0.5 * w * h
      break
    case shapeType.square:
      area = w * h 
  }
  console.log(area)
}

caclArea(shapeType.triangle, 20, 30)

```
