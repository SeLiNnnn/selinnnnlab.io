---
title: 常用Hook 
date: 2021-05-14 
tags:
    - TypeScript 
    - React 
categories:
    - Note
---

# 常用Hook

> Hook 是 React 16.8 的新增特性。Hook 是一些可以让你在函数组件里“钩入” React state 及生命周期等特性的函数。Hook 不能在 class 组件中使用 —— 这使得你不使用 class 也能使用 React。

```
// 使用use开头命名hook
// hook中的state是完全隔离的 即使是在同一个组件中使用
```

## Hook使用规则

Hook 就是 JavaScript 函数，但是使用它们会有两个额外的规则：

- 只能在**函数最外层**调用 Hook。不要在循环、条件判断或者子函数中调用。
- 只能在 **React 的函数组件和自定义Hook**中调用 Hook。不要在其他 JavaScript 函数中调用。

###  useState

`useState` 就是一个 *Hook，*通过在函数组件里调用它来给组件添加一些内部 state。React 在重复渲染时保留这个 state。`useState` 会返回一对值：**当前**
状态和一个让你更新它的函数，可以在事件处理函数中或其他一些地方调用这个函数。它类似 class 组件的 `this.setState`，但是它不会把新的 state 和旧的 state 进行合并。

```tsx
import React, {useState} from 'react';

const Hello: React.FC = () => {
// 声明一个叫做"like"的state变量 使用setLike方法修改like的值
  const [like, setLike] = useState(0);
  return (
    <section>
      <h2>
        Hooks-useState
      </h2>
      <button onClick={
        () => {
          setLike(like + 1);
        }
      }> {like} Like 👍🏻
      </button>
    </section>
  );
};

export default Hello;
```

效果如图:

![/images/常用Hook/Untitled.png](/images/常用Hook/Untitled.png)

`useState` 唯一的参数就是初始 state。在上面的例子中，like是从0开始的，所以初始 state 就是 `0`。不同于 `this.state`，这里的 state 不一定要是一个对象，这个初始state
参数只有在第一次渲染时会被用到。

与之对应的class写法：

```tsx
import React from 'react';

export class ClassUseState extends React.Component<any, any> {
  constructor(props: {} | Readonly<{}>) {
    super(props);
    this.state = {
      like: 0
    };
  }

  render() {
    return (
      <section>
        <button onClick={() => {
          this.setState({like: this.state.like + 1});
        }}>已有 {this.state.like} 赞 👍🏻
        </button>
      </section>
    );
  }
}
```

效果如图：

![/images/常用Hook/Untitled%201.png](/images/常用Hook/Untitled%201.png)

##  useEffect

在 React 组件中执行过数据获取、订阅或者手动修改过 DOM，这些操作称为“副作用"。

useEffect 就是一个 Effect Hook，给函数组件增加了操作副作用的能力。它跟 class 组件中的 componentDidMount、componentDidUpdate 和 componentWillUnmount
具有相同的用途，只不过被合并成了一个 API。

调用 useEffect 时，React 会在完成对 DOM 的更改后运行“副作用”函数。由于副作用函数是在组件内声明的，所以它们可以访问到组件的 props 和 state。默认情况下，React 会在每次渲染后调用副作用函数 ——
包括第一次渲染的时候，React 保证了每次运行 effect 的同时，DOM 都已经更新完毕。

副作用函数还可以通过返回一个函数来指定如何“清除”副作用。（如下在return中清除对鼠标移动事件的监听）

### 无需清除的 effect

有时候，我们只想**在 React 更新 DOM 之后运行一些额外的代码。**比如发送网络请求，手动变更 DOM，记录日志，这些都是常见的无需清除的操作。因为我们在执行完这些操作之后，就可以忽略他们了。

### 需要清除的 effect

有一些副作用是需要清除的。例如**订阅外部数据源**。这种情况下，清除工作是非常重要的，可以防止引起内存泄露！

### 通过跳过 Effect 进行性能优化

(示例来自react官方文档)

每次渲染后都执行清理或者执行 effect 可能会导致性能问题。在 class 组件中，我们可以通过在 componentDidUpdate 中添加对 prevProps 或 prevState 的比较逻辑解决：

```tsx
componentDidUpdate(prevProps, prevState)
{
  if (prevState.count !== this.state.count) {
    document.title = `You clicked ${this.state.count} times`;
  }
}
```

如果某些特定值在两次重渲染之间没有发生变化，你可以通知 React 跳过对 effect 的调用，只要传递数组作为 useEffect 的第二个可选参数即可：

```tsx
useEffect(() => {
  document.title = `You clicked ${count} times`;
}, [count]); // 仅在 count 更改时更新
```

🌰 :

```tsx
import React, {useEffect, useState} from 'react';

interface IPosition {
  x: number;
  y: number
}

const UseEffect: React.FC = () => {
    const [position, setPosition] = useState<IPosition>({x: 0, y: 0});
    const changePosition = (evt: MouseEvent) => {
      setPosition({x: evt.clientX, y: evt.clientY});
    };
    useEffect(() => {
      console.log('add effect');
      document.addEventListener('mousemove', changePosition);
      console.log('before render');
      return () => {
        document.removeEventListener('mousemove', changePosition);
        console.log('remove effect');
      };
    }, []);
// If you want to run an effect and clean it up only once (on mount and unmount), 
// you can pass an empty array ([]) as a second argument.

    return (
      <section>
        <h3>Use Effect</h3>
        <p>Position X: {position.x} Y: {position.y} </p>
      </section>
    );
  }
;

export default UseEffect;
```

效果如图：

![/images/常用Hook/Untitled%202.png](/images/常用Hook/Untitled%202.png)

与之对应的class写法:

```tsx
import React from 'react';

export class ClassUseEffect extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      position: {
        x: 0,
        y: 0
      }
    };
  }

  changePosition = (evt: MouseEvent) => {
    this.setState({
      position: {
        x: evt.clientX,
        y: evt.clientY
      }
    });
  };

  componentDidMount() {
    console.log('did mount');
    document.addEventListener('mousemove', this.changePosition);
  }

  componentWillUnmount() {
    console.log('will un mount');
    document.removeEventListener('mousemove', this.changePosition);
  }

  render() {
    return (
      <section>
        Class Position X:{this.state.position.x} Y: {this.state.position.y}
      </section>
    );
  }
}
```

> 与 componentDidMount 或 componentDidUpdate 不同，使用 useEffect 调度的 effect 不会阻塞浏览器更新屏幕，这让应用看起来响应更快。大多数情况下，effect 不需要同步地执行。在个别情况下（例如测量布局），有单独的 useLayoutEffect Hook 供你使用，其 API 与 useEffect 相同。

##  useContext

接收一个 context 对象（React.createContext 的返回值）并返回该 context 的当前值。当前的 context 值由上层组件中距离当前组件最近的 <MyContext.Provider> 的 value
prop 决定。

调用了 useContext 的组件总会在 context 值变化时重新渲染。如果重渲染组件的开销较大，你可以 通过使用 memoization 来优化。

```tsx
import React, {useState} from 'react';
import {ClassUseState} from './components/Class/ClassUseState/ClassUseState';
import {ClassUseEffect} from './components/Class/ClassUseState/ClassUseEffect';
import UseContext from './components/Hooks/UseContext/UseContext';

interface IThemeProps {
  [key: string]: { color: string; background: string }
}

const themes: IThemeProps = {
  'light': {
    color: '#000',
    background: '#eee'
  },
  'dark': {
    color: '#fff',
    background: '#222'
  }
};

export const ThemeContext = React.createContext(themes.light);

const App: React.FC = () => {

  const [style, setStyle] = useState(themes.light);
  return (
    <div className="App">
      <header className="App-header">
        {/*  use Context*/}
        <ThemeContext.Provider value={style}>
          <button onClick={() => {
            setStyle(style === themes.light ? themes.dark : themes.light);
          }}>点击变色
          </button>
          <UseContext/>
        </ThemeContext.Provider>
      </header>
    </div>
  );
};

export default App;
```

```tsx
import React, {useContext} from 'react';
import {ThemeContext} from '../../../App';

const UseContext: React.FC = () => {
  const theme = useContext(ThemeContext);

  return (
    <section style={theme}>
      我变色了！
    </section>
  );
};

export default UseContext;
```

效果如图：

![/images/常用Hook/Untitled%203.png](/images/常用Hook/Untitled%203.png)

![/images/常用Hook/Untitled%204.png](/images/常用Hook/Untitled%204.png)

##  useRef

useRef 返回一个可变的 ref 对象，其 .current 属性被初始化为传入的参数（initialValue）。返回的 ref 对象在组件的整个生命周期内保持不变。

1. 用于获取DOM元素

```tsx
import React, {useEffect, useRef} from 'react';

const UseRef: React.FC = () => {
  const inputRef = useRef<HTMLInputElement>(null);

// 组件渲染时聚焦
  useEffect(() => {
    if (inputRef && inputRef.current) {
      inputRef.current.focus();
    }
  });

  return (
    <section>
      <input type="text" placeholder="输入文字..." ref={inputRef}/>
    </section>
  );
};

export default UseRef;
```

2.获取状态

```tsx
import React, {useRef, useState} from 'react';

const UseRef2: React.FC = () => {
  const likeRef = useRef(0);
  const [like, setLike] = useState(0);


  // 等待弹窗期间一直触发setLike点击事件 最终弹出初始值 使用likeRef则会弹出点击后的值
  function handleAlert() {
    setTimeout(() => {
      alert('like= ' + like + ' likeRef= ' + likeRef.current);
    }, 2000);
  }

  return (
    <section>
      <button onClick={() => {
        setLike(like + 1);
        likeRef.current++;
      }
      }>{like}👍🏻
      </button>
      <button onClick={handleAlert}>alert</button>
    </section>
  );
};

export default UseRef2;

```

效果如图：
![/images/常用Hook/hooks7.png](/images/常用Hook/hooks7.png)


###  Custom Hook

通过自定义 Hook，可以将组件逻辑提取到可重用的函数中。

🌰 : 做一个页面请求时，展示loading和success的效果，并可传入deps更新状态，使用自定义Hook以便在其他组件中使用。

```tsx
import React, {useState} from 'react';
import UseState from './components/Hooks/UseState/UseState'
import useLoading from './components/Hooks/UseLoading/useLoading';

interface IShowResult {
  message: string,
  status: string
}

const App: React.FC = () => {

  const [refresh, setRefresh] = useState(true);
  const [data, loading] = useLoading('https://dog.ceo/api/breeds/image/random', [refresh]);
  const dogData = data as IShowResult;

  return (
    <div className="App">
      <header className="App-header">
        {/*  custom hook*/}
        <section>
          <div>
            <button onClick={() => {
              setRefresh(!refresh);
            }}>刷新图片
            </button>
          </div>
          {loading ?
            <p>🐶读取中</p>
            :
            <img src={dogData && dogData.message} alt="dog"/>
          }
        </section>
      </header>
    </div>
  );
};

export default App;
```

```tsx
import axios from 'axios';
import {useEffect, useState} from 'react';

export const useLoading = (url: string, deps: any[] = []) => {
  const [data, setData] = useState<any>(null);
  const [loading, setLoading] = useState<any>(false);

  useEffect(() => {
    setLoading(true);
    axios(url)
      .then((res) => {
        setLoading(false);
        setData(res.data);
      });
  }, deps);

  return [data, loading];

};

export default useLoading;
```

效果如图：

![/images/常用Hook/Untitled%205.png](/images/常用Hook/Untitled%205.png)

![/images/常用Hook/Untitled%206.png](/images/常用Hook/Untitled%206.png)

[完整Demo请戳：✈️直达](https://github.com/SeLiNnnn/react-with-ts)
