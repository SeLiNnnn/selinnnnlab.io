---
title: 浅谈JavaScript事件流
date: 2021-07-22 23:48:44
tags:
- JavaScript
- 事件流  
categories:
- Note
---

> 今天本来是有一位朋友在让我帮他押题做复习，没想到我自己提出的问题，自己都无法很好的回答，查了一些资料后，发现对这部分的知识还有很多遗漏的地方，特此记录下来

## 一、DOM Event Order

说到事件流，其实也是面试中常问的问题：

**用户点击element2，事件捕获和冒泡哪个先执行？换句话说，事件执行顺序是怎样的？**

```text
-----------------------------------
| element1                        |
|   -------------------------     |
|   |element2               |     |
|   -------------------------     |
|                                 |
-----------------------------------
```

要回答这个问题，我们先从最基本的事件流说起。

## 二、Two Models

在过去糟糕的日子里，Netscape 和微软得出了不同的结论。

> Netscape 说 element1 上的事件首先发生。这称为事件捕获。
微软坚持认为 element2 上的事件优先。这称为事件冒泡。

这两个事件顺序是完全对立的。Explorer 仅支持事件冒泡。Mozilla、Opera 7 和 Konqueror 都支持。较旧的 Opera 和 iCab 都不支持

### 1. 事件捕获Event capturing

```text
               | |
---------------| |-----------------
| element1     | |                |
|   -----------| |-----------     |
|   |element2  \ /          |     |
|   -------------------------     |
|        Event CAPTURING          |
-----------------------------------
```

element1 的事件处理程序首先触发，element2 的事件处理程序最后触发。

### 2. 事件冒泡Event bubbling

```text
               / \
---------------| |-----------------
| element1     | |                |
|   -----------| |-----------     |
|   |element2  | |          |     |
|   -------------------------     |
|        Event BUBBLING           |
-----------------------------------
```

element2 的事件处理程序首先触发，element1 的事件处理程序最后触发。

### 3. W3C model

在W3C 事件模型中发生的任何事件首先被捕获，直到它到达目标元素，然后再次冒泡。

```text
                 | |  / \
-----------------| |--| |-----------------
| element1       | |  | |                |
|   -------------| |--| |-----------     |
|   |element2    \ /  | |          |     |
|   --------------------------------     |
|        W3C event model                 |
------------------------------------------
```

再来一张图:

![event-order.png](/images/浅谈JavaScript事件流/event-order.png)

事件对象将通过 DOM 树传播，由 DOM 事件流决定事件对象的传播路径。一旦确定了传播路径，事件对象就会通过一个或多个事件阶段。

有三个事件阶段：捕获阶段、目标阶段和冒泡阶段。事件对象如下所述完成这些阶段，如果不支持某个阶段，或者事件对象的传播已停止，则将跳过该阶段。

例如，如果该bubbles属性设置为 false，则将跳过冒泡阶段，如果stopPropagation()在调度之前已调用，则将跳过所有阶段。

- **捕获阶段**：通过从目标的祖先window对象开始，传播到目标元素的父元素。(图中红色虚线）
- **目标阶段**：本次活动对象到达事件对象的目标事件，此阶段也称为*在目标阶段*。如果事件类型(click/mouseover等)表明该事件没有冒泡，则该事件对象将在此阶段完成后暂停。(图中红色实线)
- **冒泡阶段**：通过以相反的顺序，在目标对象的祖先事件对象中传播，在目标元素中开始，在window对象中结束。(图中绿色虚线部分)

## 三、事件监听

在了解了事件流过程后，如果想改变事件传播的路径，应该怎么做呢？JS中为我们提供了EventTarget.addEventLIstener()方法。

MDN中定义：

EventTarget.addEventListener() 方法将指定的监听器注册到 EventTarget 上，当该对象触发指定的事件时，指定的回调函数就会被执行。 事件目标可以是一个文档上的元素 Element,Document和Window或者任何其他支持事件的对象 (比如 XMLHttpRequest)。

addEventListener()的工作原理是将实现EventListener的函数或对象添加到调用它的EventTarget上的指定事件类型的事件侦听器列表中。

语法：

```javascript
target.addEventListener(type, listener, options);
target.addEventListener(type, listener, useCapture);
```

方法中的useCapture，就能解决刚刚我们的疑问了。

**`useCapture:Boolean`   可选**

在DOM树中，注册了listener的元素， 是否要先于它下面的EventTarget，调用该listener。 当useCapture(设为true) 时，沿着DOM树向上冒泡的事件，不会触发listener。当一个元素嵌套了另一个元素，并且两个元素都对同一事件注册了一个处理函数时，所发生的事件冒泡和事件捕获是两种不同的事件传播方式。事件传播模式决定了元素以哪个顺序接收事件。如果没有指定， `useCapture` 默认为 false 。

文字好像有点晦涩难懂，那继续来看几个示例，就能更好的理解了。

还是刚才的两个元素，ele1和ele2，如果我们这样设置:

```javascript
element1.addEventListener('click',doSomething2,true) 
element2.addEventListener('click',doSomething,false)
```

如果用户单击 element2，则会发生以下情况：

1. 该`click onclick`事件发生在捕获阶段。该事件查看 element2 的任何祖先元素是否具有

   捕获阶段的事件处理程序。

2. 该事件在 element1 上找到一个。`doSomething2()`

   被执行。

3. 事件向下传播到目标本身，没有找到捕获阶段的更多事件处理程序。事件移动到它的冒泡阶段并执行`doSomething()`，它在冒泡阶段注册到 element2。
4. 事件再次向上传播并检查目标的任何祖先元素是否具有冒泡阶段的事件处理程序。情况并非如此，所以什么也没有发生。

反过来:

```javascript
element1.addEventListener('click',doSomething2,false) 
element2.addEventListener('click',doSomething,false)
```

现在，如果用户单击 element2，则会发生以下情况：

1. 该`click onclick`事件发生在捕获阶段。该事件查看 element2 的任何祖先元素是否具有

   捕获阶段的事件处理程序，但未找到任何事件处理程序。

2. 事件向下传播到目标本身。事件移动到它的冒泡阶段并执行`doSomething()`，它在冒泡阶段注册到 element2。
3. 事件再次向上传播并检查目标的任何祖先元素是否具有冒泡阶段的事件处理程序。
4. 该事件在 element1 上找到一个。现在`doSomething2()`被执行了。

看完示例应该已经明白了整个过程的执行顺序，可简单理解为：

点击目标元素→ 从外到内(捕获) → 为true 则先执行外层的捕获事件 → 没有则执行到本身目标元素事件 → 向外冒泡 → 没有捕获阻止冒泡，则此阶段逐步执行外层事件

## 四、 与传统模型的兼容性

在支持 W3C DOM 的浏览器中，传统的事件注册

`element1.onclick = doSomething2;`

被视为冒泡阶段的注册。

### 1. **It always happens**

也许我们在日常的使用中，不常去注意到事件捕获和冒泡，但其实，事件捕获或冒泡总是会发生的。如果我们为整个文档定义了一个通用的 onclick 事件处理程序:

```javascript
document.onclick = doSomething; 
if (document.captureEvents) document.captureEvents(Event.CLICK);
```

click文档中任何元素上的任何事件最终都会冒泡到文档中，从而触发这个通用事件处理程序。只有当先前的事件处理脚本明确命令事件停止冒泡时，它才不会传播到文档。

### 2. **用途**

因为任何事件都在文档上结束，所以默认事件处理程序成为可能。假设有这样的一个页面：

```text
------------------------------------
| document                         |
|   ---------------  ------------  |
|   | element1    |  | element2 |  |
|   ---------------  ------------  |
|                                  |
------------------------------------
```

```javascript
element1.onclick = doSomething;
element2.onclick = doSomething;
document.onclick = defaultFunction;
```

点击element1或element2，不希望触发document的事件，在 Microsoft 模型中，可将事件的cancelBubble属性设置为 true。

在 W3C 模型中，使用调用事件的stopPropagation()方法。

**跨浏览取消事件冒泡：**

```javascript
function doSomething(e) { 
	if (!e) var e = window.event; 
	e.cancelBubble = true; 
	if (e.stopPropagation) e.stopPropagation(); 
}
```

### 3. 当前目标

正如我们之前看到的，一个事件有一个`target`or `srcElement`，它包含对发生事件的元素的引用。在我们的示例中，这是 element2，因为用户单击了它。

理解在捕获和冒泡阶段（如果有的话）这个目标不会改变是非常重要的：它始终保持对 element2 的引用。

但是假设我们注册了这些事件处理程序：

```javascript
element1.onclick = doSomething; 
element2.onclick = doSomething;
```

如果用户单击 element2，`doSomething()`则执行两次。但是你怎么知道哪个 HTML 元素当前正在处理这个事件呢？`target/srcElement` 不会给出线索，它们总是指 element2，因为它是事件的原始来源。

为了解决这个问题，W3C 添加了这个`currentTarget`属性。它包含对当前正在处理事件的 HTML 元素的引用：正是我们所需要的。不幸的是，Microsoft 模型不包含类似的属性。

还可以使用`this`关键字。在上面的例子中，它指的是处理事件的 HTML 元素，就像 `currentTarget`.

****需要注意的是，使用 Microsoft 事件注册模型时，this关键字并不指代 HTML 元素，无法知道当前处理该事件的HTML元素，这是 Microsoft 事件注册模型最严重的问题。***


好啦，事件流已经介绍完毕啦，也算是自己的一次复习巩固~

之后再开个坑写写事件委托吧，毕竟我的js是真的菜，需要学习的地方还有很多。💪🏻


**相关参考文章链接请戳✈️ ：**

[EventTarget.addEventListener()](https://developer.mozilla.org/zh-CN/docs/Web/API/EventTarget/addEventListener)

[事件流](https://www.w3.org/TR/DOM-Level-3-Events/#event-flow)

[JavaScript Event Order](https://www.quirksmode.org/js/events_order.html#link4)
